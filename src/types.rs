use alloc::string::String;
use alloc::vec::Vec;
use core::fmt::{Debug, Formatter};
use core::ops::{Deref, DerefMut};

use serde_derive::{Deserialize, Serialize};

use crate::error::{Error, Result};

/// A no-std replacement for the std::io Write trait
pub trait Write {
    /// The type of error returned when a write operation fails.
    type Error: Into<Error>;

    /// Attempts to write an entire buffer into this write.
    fn write_all(&mut self, buf: &[u8]) -> Result<()>;
}

/// A no-std replacement for the std::io Read trait
pub trait Read {
    /// The type of error returned when a write operation fails.
    type Error: Into<Error>;

    /// Attempts to read into the buffer, returning the number of bytes read
    fn read(&mut self, dest: &mut [u8]) -> Result<usize>;

    /// Peeks one byte
    fn peek(&mut self) -> Result<Option<u8>>;
}

/// Vec<u8> with nicer debug formatting
/// Maximum 65535 bytes, for larger data use `LargeBytes`
#[derive(Serialize, Deserialize, PartialEq)]
pub struct Octets(pub Vec<u8>);

impl Octets {
    /// An empty vector
    pub const EMPTY: Self = Octets(Vec::new());
}

impl Debug for Octets {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str(&hex::encode(&self.0))
    }
}

impl Deref for Octets {
    type Target = Vec<u8>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Octets {
    fn deref_mut(&mut self) -> &mut Vec<u8> {
        &mut self.0
    }
}

impl From<Vec<u8>> for Octets {
    fn from(v: Vec<u8>) -> Self {
        Self(v)
    }
}

/// A potentially large vector of bytes, with a u32 size
///
/// Not used in BOLT-1, because messages are limited to 64 KB
#[cfg_attr(test, derive(PartialEq))]
#[derive(Serialize, Deserialize)]
pub struct LargeOctets(pub Vec<u8>);

impl LargeOctets {
    /// An empty vector
    pub const EMPTY: Self = LargeOctets(Vec::new());
    pub(crate) const NAME: &'static str = "LargeOctets";
}

impl Debug for LargeOctets {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str(&hex::encode(&self.0))
    }
}

impl Deref for LargeOctets {
    type Target = Vec<u8>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for LargeOctets {
    fn deref_mut(&mut self) -> &mut Vec<u8> {
        &mut self.0
    }
}

impl From<Vec<u8>> for LargeOctets {
    fn from(v: Vec<u8>) -> Self {
        Self(v)
    }
}

/// A BOLT compatible variable-size compact integer representation
pub struct BigSize(pub u64);

impl BigSize {
    /// Write
    #[inline]
    pub fn write<W: Write>(&self, writer: &mut W) -> Result<()> {
        match self.0 {
            0..=0xFC => writer.write_all(&[self.0 as u8]),
            0xFD..=0xFFFF => {
                writer.write_all(&[0xFDu8])?;
                writer.write_all(&(self.0 as u16).to_be_bytes())
            }
            0x10000..=0xFFFFFFFF => {
                writer.write_all(&[0xFEu8])?;
                writer.write_all(&(self.0 as u32).to_be_bytes())
            }
            _ => {
                writer.write_all(&[0xFFu8])?;
                writer.write_all(&(self.0 as u64).to_be_bytes())
            }
        }
    }
}

/// A variable length zero terminated byte string
#[cfg_attr(test, derive(PartialEq))]
#[derive(Serialize, Deserialize)]
pub struct WireString(pub Vec<u8>);

impl WireString {
    pub(crate) const NAME: &'static str = "WireString";
}

impl Debug for WireString {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        match String::from_utf8(self.0.clone()) {
            Ok(str) => write!(f, "\"{}\"", str),             // utf8
            Err(_) => write!(f, "{}", hex::encode(&self.0)), // non-uf8
        }
    }
}

/// A variable-length integer which is simply truncated by skipping high zero bytes.
///
/// A BOLT structure may have at most one of these, and it must come at the end.
#[cfg_attr(test, derive(PartialEq, Debug))]
#[derive(Serialize, Deserialize)]
pub struct HighZeroBytesDroppedVarInt<T>(pub T);

impl HighZeroBytesDroppedVarInt<()> {
    pub(crate) const NAME: &'static str = "HighZeroBytesDroppedVarInt";
}

macro_rules! impl_writeable_primitive {
    ($val_type:ty, $len: expr) => {
        impl HighZeroBytesDroppedVarInt<$val_type> {
            /// Write
            #[inline]
            pub fn write<W: Write>(&self, writer: &mut W) -> $crate::Result<()> {
                // Skip any full leading 0 bytes when writing (in BE):
                writer.write_all(&self.0.to_be_bytes()[(self.0.leading_zeros() / 8) as usize..$len])
            }
        }

        impl HighZeroBytesDroppedVarInt<$val_type> {
            /// Read
            #[inline]
            pub fn read<R: Read>(
                reader: &mut R,
            ) -> $crate::Result<HighZeroBytesDroppedVarInt<$val_type>> {
                // We need to accept short reads (read_len == 0) as "EOF" and handle them as simply
                // the high bytes being dropped. To do so, we start reading into the middle of buf
                // and then convert the appropriate number of bytes with extra high bytes out of
                // buf.
                let mut buf = [0; $len * 2];
                let mut read_len = reader.read(&mut buf[$len..])?;
                let mut total_read_len = read_len;
                while read_len != 0 && total_read_len != $len {
                    read_len = reader.read(&mut buf[($len + total_read_len)..])?;
                    total_read_len += read_len;
                }
                if total_read_len == 0 || buf[$len] != 0 {
                    let first_byte = $len - ($len - total_read_len);
                    let mut bytes = [0; $len];
                    bytes.copy_from_slice(&buf[first_byte..first_byte + $len]);
                    Ok(HighZeroBytesDroppedVarInt(<$val_type>::from_be_bytes(
                        bytes,
                    )))
                } else {
                    // If the encoding had extra zero bytes, return a failure even though we know
                    // what they meant (as the TLV test vectors require this)
                    Err($crate::Error::InvalidVarInt)
                }
            }
        }
    };
}

impl_writeable_primitive!(u64, 8);
impl_writeable_primitive!(u32, 4);
impl_writeable_primitive!(u16, 2);

#[cfg(test)]
mod tests {
    use hex::{decode, encode};

    use crate::Error;
    use crate::LargeOctets;
    use crate::WireString;

    use super::HighZeroBytesDroppedVarInt;

    #[test]
    fn test_tu32() {
        let v = HighZeroBytesDroppedVarInt(0x1234u32);
        let mut encoded = Vec::new();
        v.write(&mut &mut encoded).unwrap();
        assert_eq!("1234", encode(&encoded));
        let dv = HighZeroBytesDroppedVarInt::<u32>::read(&mut encoded).unwrap();
        assert_eq!(dv, v);
    }

    #[test]
    fn test_tu32_non_standard() {
        let mut encoded = decode("123400").unwrap();
        assert_eq!(
            HighZeroBytesDroppedVarInt::<u32>::read(&mut encoded)
                .unwrap()
                .0,
            0x123400u32
        );
        let mut encoded = decode("001234").unwrap();
        assert_eq!(
            HighZeroBytesDroppedVarInt::<u32>::read(&mut encoded),
            Err(Error::InvalidVarInt)
        );
    }

    #[test]
    fn test_debug() {
        assert_eq!(format!("{:?}", LargeOctets(vec![0, 1, 2, 3,])), "00010203");
        // wirestring w/ non-utf8 is hex
        assert_eq!(
            format!("{:?}", WireString(vec![0xC0, 5, 6, 7,])),
            "c0050607"
        );
        // otherwise convert wirestring to utf8
        assert_eq!(
            format!("{:?}", WireString(vec![0x62, 0x63, 0x72, 0x74,])),
            "\"bcrt\""
        );
    }
}
