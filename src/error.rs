use alloc::format;
use alloc::string::String;
use core::fmt::{self, Display};
use serde::{de, ser};

/// Result
pub type Result<T> = core::result::Result<T, Error>;

/// Possible errors
#[derive(Clone, Debug, PartialEq)]
pub enum Error {
    /// A custom message error
    Message(String),

    /// EOF was reached while trying to de-serialize an expected field
    Eof,
    /// A boolean was expected, but the byte was greater than 1
    ExpectedBoolean,
    /// Invalid varint
    InvalidVarInt,
    /// There can be at most one variable field in a struct, and it must be at end
    VariableFieldNotAtEnd,
}

impl ser::Error for Error {
    fn custom<T: Display>(msg: T) -> Self {
        Error::Message(format!("{}", msg))
    }
}

impl de::Error for Error {
    fn custom<T: Display>(msg: T) -> Self {
        Error::Message(format!("{}", msg))
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Message(msg) => write!(f, "{}", msg),
            Error::Eof => f.write_str("unexpected end of input"),
            /* and so forth */
            _ => unimplemented!(),
        }
    }
}

#[cfg(feature = "std")]
impl std::error::Error for Error {}
