use alloc::vec;
use alloc::vec::Vec;

use serde::de::{self, Deserialize, DeserializeSeed, Visitor};

use crate::error::{Error, Result};
use crate::types::{HighZeroBytesDroppedVarInt, LargeOctets, Read, WireString};

impl Read for Vec<u8> {
    type Error = Error;

    fn read(&mut self, dest: &mut [u8]) -> Result<usize> {
        if dest.len() >= self.len() {
            dest[0..self.len()].copy_from_slice(self);
            let n = self.len();
            self.drain(0..self.len());
            Ok(n)
        } else {
            dest.copy_from_slice(&self[0..dest.len()]);
            self.drain(0..dest.len());
            Ok(dest.len())
        }
    }

    fn peek(&mut self) -> Result<Option<u8>> {
        if self.is_empty() {
            return Ok(None);
        }
        Ok(Some(self[0]))
    }
}

/// BOLT deserializer
pub struct Deserializer<'de, R: Read> {
    reader: &'de mut R,
    saw_variable_field: bool,
    in_varint: bool,
    in_large_octets: bool,
    in_string: bool,
}

impl<'de, R: Read> Deserializer<'de, R> {
    /// Deserialize from reader
    pub fn from_reader(reader: &'de mut R) -> Self {
        Deserializer {
            reader,
            saw_variable_field: false,
            in_varint: false,
            in_large_octets: false,
            in_string: false,
        }
    }
}

/// Deserialize from Vec
pub fn from_vec<'a, T>(s: &'a mut Vec<u8>) -> Result<T>
where
    T: Deserialize<'a>,
{
    from_reader(s)
}

/// Deserialize from reader
pub fn from_reader<'a, T, R: Read>(r: &'a mut R) -> Result<T>
where
    T: Deserialize<'a>,
{
    let mut deserializer = Deserializer::from_reader(r);
    let t = T::deserialize(&mut deserializer)?;
    Ok(t)
}

impl<'de, 'a, R: Read> de::Deserializer<'de> for &'a mut Deserializer<'de, R> {
    type Error = Error;

    fn deserialize_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        let result = self.read_bool()?;
        visitor.visit_bool(result)
    }

    fn deserialize_i8<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_i16<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_i32<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_i64<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        let mut result = [0u8; 1];
        let n = self.reader.read(&mut result)?;
        if n < result.len() {
            return Err(Error::Eof);
        }
        visitor.visit_u8(result[0])
    }

    fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        if self.in_varint {
            self.saw_variable_field = true;
            self.in_varint = false;
            let v = HighZeroBytesDroppedVarInt::<u16>::read(self.reader)?;
            visitor.visit_u16(v.0)
        } else {
            let mut result = [0u8; 2];
            let n = self.reader.read(&mut result)?;
            if n < result.len() {
                return Err(Error::Eof);
            }
            visitor.visit_u16(u16::from_be_bytes(result))
        }
    }

    fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        if self.in_varint {
            self.saw_variable_field = true;
            self.in_varint = false;
            let v = HighZeroBytesDroppedVarInt::<u32>::read(self.reader)?;
            visitor.visit_u32(v.0)
        } else {
            let mut result = [0u8; 4];
            let n = self.reader.read(&mut result)?;
            if n < result.len() {
                return Err(Error::Eof);
            }
            visitor.visit_u32(u32::from_be_bytes(result))
        }
    }

    fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        if self.in_varint {
            self.saw_variable_field = true;
            self.in_varint = false;
            let v = HighZeroBytesDroppedVarInt::<u64>::read(self.reader)?;
            visitor.visit_u64(v.0)
        } else {
            let mut result = [0u8; 8];
            let n = self.reader.read(&mut result)?;
            if n < result.len() {
                return Err(Error::Eof);
            }
            visitor.visit_u64(u64::from_be_bytes(result))
        }
    }

    fn deserialize_f32<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_f64<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_char<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_str<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_string<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let len = self.read_u16()?;
        let mut buf = vec![0u8; len as usize];
        let n = self.reader.read(&mut buf)?;
        if n < buf.len() {
            return Err(Error::Eof);
        }
        visitor.visit_bytes(&buf)
    }

    fn deserialize_byte_buf<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_option<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        let result = self.read_bool()?;

        if result {
            visitor.visit_some(self)
        } else {
            visitor.visit_none()
        }
    }

    fn deserialize_unit<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit_struct<V>(self, _name: &'static str, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_newtype_struct<V>(mut self, name: &'static str, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        if name == HighZeroBytesDroppedVarInt::NAME {
            self.in_varint = true;
            visitor.visit_newtype_struct(self)
        } else if name == LargeOctets::NAME {
            self.in_large_octets = true;
            visitor.visit_newtype_struct(self)
        } else if name == WireString::NAME {
            self.in_string = true;
            visitor.visit_newtype_struct(self)
        } else {
            visitor.visit_newtype_struct(self)
        }
    }

    fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        if self.in_string {
            self.in_string = false;
            visitor.visit_seq(StringDeser::new(self))
        } else if self.in_large_octets {
            self.in_large_octets = false;
            let size = self.read_u32()?;
            visitor.visit_seq(SeqDeser::new_large(self, size))
        } else {
            let size = self.read_u16()?;
            visitor.visit_seq(SeqDeser::new(self, size))
        }
    }

    fn deserialize_tuple<V>(self, len: usize, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        visitor.visit_seq(SeqDeser::new(self, len as u16))
    }

    fn deserialize_tuple_struct<V>(
        self,
        _name: &'static str,
        _len: usize,
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.deserialize_seq(visitor)
    }

    fn deserialize_map<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_struct<V>(
        mut self,
        _name: &'static str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.ensure_did_not_see_variable_field()?;
        let fields_vec = Vec::from(fields);
        visitor.visit_seq(StructDeser::new(&mut self, fields_vec))
    }

    fn deserialize_enum<V>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        _visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_identifier<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_ignored_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn is_human_readable(&self) -> bool {
        // prevent value serializers from reading as hex string instead of bytes
        false
    }
}

struct StructDeser<'a, 'de: 'a, R: Read> {
    de: &'a mut Deserializer<'de, R>,
    fields: Vec<&'static str>,
}

impl<'a, 'de, R: Read> StructDeser<'a, 'de, R> {
    fn new(de: &'a mut Deserializer<'de, R>, fields: Vec<&'static str>) -> Self {
        StructDeser { de, fields }
    }
}

impl<'a, 'de, R: Read> de::SeqAccess<'de> for StructDeser<'a, 'de, R> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        let next_field_opt = self.fields.pop();
        if next_field_opt.is_some() {
            seed.deserialize(&mut *self.de).map(Some)
        } else {
            Ok(None)
        }
    }
}

struct SeqDeser<'a, 'de: 'a, R: Read> {
    de: &'a mut Deserializer<'de, R>,
    size: u32,
}

impl<'a, 'de, R: Read> SeqDeser<'a, 'de, R> {
    fn new_large(de: &'a mut Deserializer<'de, R>, size: u32) -> Self {
        SeqDeser { de, size }
    }

    fn new(de: &'a mut Deserializer<'de, R>, size: u16) -> Self {
        SeqDeser {
            de,
            size: size as u32,
        }
    }
}

impl<'a, 'de, R: Read> de::SeqAccess<'de> for SeqDeser<'a, 'de, R> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        if self.size > 0 {
            self.size -= 1;
            seed.deserialize(&mut *self.de).map(Some)
        } else {
            Ok(None)
        }
    }
}

struct StringDeser<'a, 'de: 'a, R: Read> {
    de: &'a mut Deserializer<'de, R>,
}

impl<'a, 'de, R: Read> StringDeser<'a, 'de, R> {
    fn new(de: &'a mut Deserializer<'de, R>) -> Self {
        StringDeser { de }
    }
}

impl<'a, 'de, R: Read> de::SeqAccess<'de> for StringDeser<'a, 'de, R> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        if self.de.peek()? == Some(0) {
            // Consume the null byte
            self.de.read_u8()?;
            return Ok(None);
        }
        seed.deserialize(&mut *self.de).map(Some)
    }
}

impl<'de, R: Read> Deserializer<'de, R> {
    fn read_bool(&mut self) -> Result<bool> {
        let result = self.read_u8()?;
        if result > 1 {
            return Err(Error::ExpectedBoolean);
        }
        Ok(result > 0)
    }

    fn read_u8(&mut self) -> Result<u8> {
        let mut result = [0u8];
        let n = self.reader.read(&mut result)?;
        if n < 1 {
            return Err(Error::Eof);
        }
        Ok(result[0])
    }

    fn peek(&mut self) -> Result<Option<u8>> {
        self.reader.peek()
    }
}

impl<'a, 'de, R: Read> Deserializer<'de, R> {
    fn ensure_did_not_see_variable_field(&mut self) -> Result<()> {
        if self.saw_variable_field {
            return Err(Error::VariableFieldNotAtEnd);
        }
        Ok(())
    }
}

impl<'de, R: Read> Deserializer<'de, R> {
    fn read_u16(&mut self) -> Result<u16> {
        let mut result = [0u8; 2];
        let n = self.reader.read(&mut result)?;
        if n < result.len() {
            return Err(Error::Eof);
        }
        let size = u16::from_be_bytes(result);
        Ok(size)
    }

    fn read_u32(&mut self) -> Result<u32> {
        let mut result = [0u8; 4];
        let n = self.reader.read(&mut result)?;
        if n < result.len() {
            return Err(Error::Eof);
        }
        let size = u32::from_be_bytes(result);
        Ok(size)
    }
}
