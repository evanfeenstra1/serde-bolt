use alloc::vec::Vec;

use serde::ser::Error as _;
use serde::ser::{self, Serialize};

use crate::error::{Error, Result};
use crate::types::{HighZeroBytesDroppedVarInt, LargeOctets, WireString, Write};

impl Write for &mut Vec<u8> {
    type Error = Error;

    fn write_all(&mut self, buf: &[u8]) -> Result<()> {
        self.append(&mut buf.to_vec());
        Ok(())
    }
}

/// Serializer
pub struct Serializer<W: Write> {
    pub(crate) writer: W,
    saw_variable_field: bool,
    in_varint: bool,
    in_large_octets: bool,
    in_string: bool,
}

impl<W: Write> Serializer<W> {
    /// Create the serializer
    pub fn new(writer: W) -> Self {
        Serializer {
            writer,
            saw_variable_field: false,
            in_varint: false,
            in_large_octets: false,
            in_string: false,
        }
    }
}

/// Serialize into a vector
pub fn to_vec<T>(value: &T) -> Result<Vec<u8>>
where
    T: ser::Serialize,
{
    let mut vec = Vec::new();
    value.serialize(&mut Serializer::new(&mut vec))?;
    Ok(vec)
}

/// Serialize into a writer
pub fn to_writer<W, T>(writer: W, value: &T) -> Result<()>
where
    T: Serialize,
    W: Write,
{
    let mut serializer = Serializer::new(writer);
    value.serialize(&mut serializer)?;
    Ok(())
}

impl<'a, W: Write> ser::Serializer for &'a mut Serializer<W> {
    type Ok = ();

    type Error = Error;

    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    fn serialize_bool(self, v: bool) -> Result<()> {
        self.ensure_did_not_see_variable_field()?;
        self.writer.write_all(&[if v { 1u8 } else { 0u8 }])
    }

    fn serialize_i8(self, _v: i8) -> Result<()> {
        unimplemented!()
    }

    fn serialize_i16(self, _v: i16) -> Result<()> {
        unimplemented!()
    }

    fn serialize_i32(self, _v: i32) -> Result<()> {
        unimplemented!()
    }

    fn serialize_i64(self, _v: i64) -> Result<()> {
        unimplemented!()
    }

    fn serialize_u8(self, v: u8) -> Result<()> {
        self.ensure_did_not_see_variable_field()?;
        self.writer.write_all(&[v])
    }

    fn serialize_u16(self, v: u16) -> Result<()> {
        self.ensure_did_not_see_variable_field()?;
        if self.in_varint {
            self.saw_variable_field = true;
            self.in_varint = false;
            HighZeroBytesDroppedVarInt(v).write(&mut self.writer)
        } else {
            self.writer.write_all(&v.to_be_bytes())
        }
    }

    fn serialize_u32(self, v: u32) -> Result<()> {
        self.ensure_did_not_see_variable_field()?;
        if self.in_varint {
            self.saw_variable_field = true;
            self.in_varint = false;
            HighZeroBytesDroppedVarInt(v).write(&mut self.writer)
        } else {
            self.writer.write_all(&v.to_be_bytes())
        }
    }

    fn serialize_u64(self, v: u64) -> Result<()> {
        self.ensure_did_not_see_variable_field()?;
        if self.in_varint {
            self.saw_variable_field = true;
            self.in_varint = false;
            HighZeroBytesDroppedVarInt(v).write(&mut self.writer)
        } else {
            self.writer.write_all(&v.to_be_bytes())
        }
    }

    fn serialize_f32(self, _v: f32) -> Result<()> {
        unimplemented!()
    }

    fn serialize_f64(self, _v: f64) -> Result<()> {
        unimplemented!()
    }

    fn serialize_char(self, _v: char) -> Result<()> {
        unimplemented!()
    }

    fn serialize_str(self, _v: &str) -> Result<()> {
        unimplemented!()
        // TODO put this back once the de-serializer is implemented
        // BigSize(v.len() as u64).write(&mut self.writer)?;
        // self.writer.write_all(&v.as_bytes())
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<()> {
        self.ensure_did_not_see_variable_field()?;
        // Unfortunately, the deserializer doesn't know the length, even if it's
        // a fixed length field, so we have to write the length.
        // We'll assume it's under 64KiB.
        // It's mostly use for Bitcoin hashes and such, which are small.
        if v.len() >= u16::MAX as usize {
            return Err(Error::custom("can't serialize too large bytes"));
        }
        self.serialize_u16(v.len() as u16)?;
        self.writer.write_all(v)
    }

    fn serialize_none(self) -> Result<()> {
        self.ensure_did_not_see_variable_field()?;
        self.writer.write_all(&[0u8])
    }

    fn serialize_some<T>(self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.ensure_did_not_see_variable_field()?;
        self.writer.write_all(&[1u8])?;
        value.serialize(self)
    }

    fn serialize_unit(self) -> Result<()> {
        unimplemented!()
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<()> {
        unimplemented!()
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
    ) -> Result<()> {
        unimplemented!()
    }

    fn serialize_newtype_struct<T>(mut self, name: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.ensure_did_not_see_variable_field()?;
        if name == HighZeroBytesDroppedVarInt::NAME {
            self.saw_variable_field = false;
            self.in_varint = true;
            value.serialize(self)
        } else if name == LargeOctets::NAME {
            self.in_large_octets = true;
            value.serialize(self)
        } else if name == WireString::NAME {
            self.in_string = true;
            value.serialize(self)
        } else {
            value.serialize(self)
        }
    }

    fn serialize_newtype_variant<T>(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        value: &T,
    ) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        // Transparent for convenience
        value.serialize(self)
    }

    fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq> {
        self.ensure_did_not_see_variable_field()?;
        if let Some(length) = len {
            if self.in_string {
                // will be turned off by end()
            } else if self.in_large_octets {
                self.in_large_octets = false;
                if length >= u32::MAX as usize {
                    return Err(Error::custom("can't serialize too large seq"));
                }
                self.serialize_u32(length as u32)?;
            } else {
                if length >= u16::MAX as usize {
                    return Err(Error::custom("can't serialize too large seq"));
                }
                self.serialize_u16(length as u16)?;
            }
            Ok(self)
        } else {
            Err(Error::custom("can't serialize seq - no length"))
        }
    }

    fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
        self.ensure_did_not_see_variable_field()?;
        Ok(self)
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleStruct> {
        unimplemented!()
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleVariant> {
        unimplemented!()
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        unimplemented!()
    }

    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        // Consider structs to be transparent containers for now
        self.ensure_did_not_see_variable_field()?;
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStructVariant> {
        unimplemented!()
    }

    fn is_human_readable(&self) -> bool {
        // prevent value serializers from writing out hex string instead of bytes
        false
    }
}

impl<'a, W: Write> ser::SerializeSeq for &'a mut Serializer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(mut self) -> Result<()> {
        if self.in_string {
            self.in_string = false;
            self.writer.write_all(&[0u8])
        } else {
            Ok(())
        }
    }
}

impl<'a, W: Write> ser::SerializeTuple for &'a mut Serializer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W: Write> ser::SerializeTupleStruct for &'a mut Serializer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W: Write> ser::SerializeTupleVariant for &'a mut Serializer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W: Write> ser::SerializeMap for &'a mut Serializer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_key<T>(&mut self, key: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        key.serialize(&mut **self)
    }

    fn serialize_value<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W: Write> ser::SerializeStruct for &'a mut Serializer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, _key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W: Write> ser::SerializeStructVariant for &'a mut Serializer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        key.serialize(&mut **self)?;
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<W: Write> Serializer<W> {
    fn ensure_did_not_see_variable_field(&mut self) -> Result<()> {
        if self.saw_variable_field {
            return Err(Error::VariableFieldNotAtEnd);
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use bitcoin::hashes::Hash;
    use bitcoin::BlockHash;
    use hex::{decode, encode};
    use serde_derive::{Deserialize, Serialize};

    use crate::{from_vec, to_vec, Error, Octets, Result};
    use crate::{HighZeroBytesDroppedVarInt, LargeOctets, WireString};

    #[derive(Serialize, Deserialize, PartialEq, Debug)]
    struct Thing([u8; 3]);

    #[derive(Serialize, Deserialize, PartialEq, Debug)]
    struct Test {
        x: bool,
        a: u32,
        b: u8,
        c: Option<u16>,
        d: Option<u16>,
        e: Vec<u8>,
        /// Sub-structs are transparent
        f: Thing,
    }

    #[derive(Serialize, Deserialize, PartialEq, Debug)]
    struct TestWithVar {
        x: bool,
        a: HighZeroBytesDroppedVarInt<u32>,
    }

    #[test]
    fn test_good() {
        let test = Test {
            x: true,
            a: 65538,
            b: 160,
            c: None,
            d: Some(3),
            e: vec![0x33, 0x44],
            f: Thing([0x55, 0x66, 0x77]),
        };
        let result = to_vec(&test).unwrap();
        assert_eq!("0100010002a00001000300023344556677", encode(&result));

        let result = to_vec(&test).unwrap();
        assert_eq!("0100010002a00001000300023344556677", encode(&result));

        let decoded: Test = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test, decoded);

        let mut result_with_trailing: Vec<u8> = [result, vec![0x88]].concat();
        let decoded: Test = from_vec(&mut result_with_trailing).unwrap();
        assert_eq!(test, decoded);
    }

    #[test]
    fn test_bad() {
        assert_eq!(
            from_vec::<Test>(&mut decode("0100010002a000010003000233445566").unwrap().clone()),
            Err(Error::Eof)
        );
        assert_eq!(
            from_vec::<Test>(
                &mut decode("0200010002a00001000300023344556677")
                    .unwrap()
                    .clone()
            ),
            Err(Error::ExpectedBoolean)
        );
    }

    #[test]
    fn test_var_good() {
        let test = TestWithVar {
            x: false,
            a: HighZeroBytesDroppedVarInt(0x12),
        };
        let result = to_vec(&test).unwrap();
        assert_eq!("0012", encode(&result));
        let decoded: TestWithVar = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test, decoded);
        let test = TestWithVar {
            x: false,
            a: HighZeroBytesDroppedVarInt(0x123456),
        };
        let result = to_vec(&test).unwrap();
        assert_eq!("00123456", encode(&result));
        let decoded: TestWithVar = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test, decoded);
    }

    #[derive(Serialize, Deserialize, PartialEq, Debug)]
    struct TestWithBadVar {
        a: HighZeroBytesDroppedVarInt<u32>,
        x: bool,
    }

    #[test]
    fn test_ser_var_not_at_end() {
        let test = TestWithBadVar {
            x: false,
            a: HighZeroBytesDroppedVarInt(0x12),
        };
        assert_eq!(to_vec(&test).err().unwrap(), Error::VariableFieldNotAtEnd);
    }

    #[test]
    fn test_de_var_not_at_end() {
        let result: Result<TestWithBadVar> = from_vec(&mut decode("1111111100").unwrap());
        assert_eq!(result.err().unwrap(), Error::VariableFieldNotAtEnd);
    }

    #[derive(Serialize, Deserialize, PartialEq, Debug)]
    struct TestWithLargeBytes {
        a: LargeOctets,
    }

    #[test]
    fn test_ser_large_octets() {
        let test = TestWithLargeBytes {
            a: LargeOctets(vec![0x11, 0x22]),
        };
        let result = to_vec(&test).unwrap();
        assert_eq!("000000021122", encode(&result));
        let decoded: TestWithLargeBytes = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test, decoded);
    }

    #[test]
    fn test_bitcoin_compat() {
        let test = BlockHash::all_zeros();
        let result = to_vec(&test).unwrap();
        let decoded: BlockHash = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test, decoded);
    }

    #[test]
    fn test_slice() {
        let test = [0x11, 0x22, 0x33, 0x44];
        let result = to_vec(&test).unwrap();
        assert_eq!("11223344", encode(&result));
        let decoded: [u8; 4] = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test, decoded);
    }

    #[derive(Serialize, Deserialize, PartialEq, Debug)]
    struct TestWithWireString {
        a: WireString,
        b: Octets,
    }

    #[test]
    fn test_ser_wire_string_and_octets() {
        let test = TestWithWireString {
            a: WireString("hello".as_bytes().to_vec()),
            b: vec![0x11u8].into(),
        };
        let result = to_vec(&test).unwrap();
        assert_eq!("68656c6c6f00000111", encode(&result));
        let decoded: TestWithWireString = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test, decoded);
    }
}
