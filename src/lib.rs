// This file is Copyright its original authors, visible in version control
// history.
//
// This file is licensed under the Apache License, Version 2.0 <LICENSE-APACHE
// or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option.
// You may not use this file except in accordance with one or both of these
// licenses.

//! serde-bolt - a Bitcoin Lightning serializer / deserializer for BOLT style messages

#![cfg_attr(all(not(feature = "std"), not(test)), no_std)]
#![deny(missing_docs)]
#![deny(unsafe_code)]

extern crate alloc;

mod de;
mod error;
mod ser;
mod types;

pub use crate::de::{from_reader, from_vec, Deserializer};
pub use crate::error::{Error, Result};
pub use crate::ser::{to_vec, to_writer, Serializer};
pub use crate::types::*;
