serde-BOLT
==============

[![Crate](https://img.shields.io/crates/v/serde_bolt.svg?logo=rust)](https://crates.io/crates/serde_bolt)
[![Documentation](https://img.shields.io/static/v1?logo=read-the-docs&label=docs.rs&message=serde_bolt&color=informational)](https://docs.rs/serde_bolt/)
[![Safety Dance](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)

An incomplete implementation of the Lightning BOLT message serialization format.

Domain-specific types are not implemented.  You can just use `Vec<u8>` or `[u8; nnn]` or a wrapped version thereof.

Structs and tuples are considered transparent - they are not delimited in the stream.

Unimplemented
-------------

* TLVs

Usage
-----

```rust
    use hex::{encode, decode};
    use serde_derive::{Serialize, Deserialize};

    use serde_bolt::{to_vec, from_vec, Error};

    #[derive(Serialize, Deserialize, PartialEq, Debug)]
    struct Thing([u8; 3]);
    
    #[derive(Serialize, Deserialize, PartialEq, Debug)]
    struct Test {
        x: bool,
        a: u32,
        b: u8,
        c: Option<u16>,
        d: Option<u16>,
        e: Vec<u8>,
        /// Sub-structs are transparent
        f: Thing
    }

    #[test]
    fn test_simple() {
        let test = Test {
            x: true,
            a: 65538, b:160, c: None, d: Some(3),
            e: vec![0x33, 0x44],
            f: Thing([0x55, 0x66, 0x77])
        };
        let result = to_vec(&test).unwrap();
        assert_eq!("0100010002a00001000300023344556677", encode(&result));
    
        let decoded: Test = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test, decoded);
    }
```
